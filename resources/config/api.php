<?php

return [
    'api-url' => env('API_URL', 'http://localhost/api'),

    'sample-api-execution-time' => 1000,
];