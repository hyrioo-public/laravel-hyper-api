<?php

namespace Hyrioo\LaravelHyperApi\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class EloquentRepository
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * Base query for API controller
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function getBaseQuery()
    {
        return $this->model->newQuery();
    }

    /**
     * Find a model by its primary key.
     *
     * @param  mixed  $id
     * @param  array  $columns
     * @return \Illuminate\Database\Query\Builder
     */
    public function findQuery($id, $columns = ['*'])
    {
        if (is_array($id)) {
            return $this->findManyQuery($id, $columns);
        }

        return $this->getBaseQuery()->where($this->model->getQualifiedKeyName(), '=', $id);
    }

    /**
     * Find multiple models by their primary keys.
     *
     * @param  array  $ids
     * @param  array  $columns
     * @return \Illuminate\Database\Query\Builder
     */
    public function findManyQuery($ids, $columns = ['*'])
    {
        if (empty($ids)) {
            return $this->getBaseQuery()->newCollection();
        }

        return $this->getBaseQuery()->whereIn($this->model->getQualifiedKeyName(), $ids);
    }





    /**
     * Get all models
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * Find model by id
     *
     * @param $id
     * @return Model
     */
    public function find($id)
    {
        return $this->model->find($id);
    }

    /**
     *
     *
     * @param string $column
     * @param string|int $value
     * @param bool $like use like in where statement
     * @return Model
     */
    public function findByColumn($column, $value)
    {
        return $this->model->where($column, $value)->first();
    }

    /**
     * Create a new instance of model
     *
     * @param mixed[] $input
     * @param bool $save
     * @return Model
     */
    public function create($input, $save=true)
    {
        $instance = $this->model->newInstance($input);
        if($save)
            $instance->save();
        return $instance;
    }

    /**
     * Edits a model
     *
     * @param mixed[] $input
     * @param int $id
     * @return Model|bool
     */
    public function edit($input, $id)
    {
        /** @var Model $instance */
        $instance = $this->model->find($id);
        if (!$instance) {
            return false;
        }
        $instance->update($input);
        $instance->save();
        return $instance;
    }

    /**
     * Delete a model by id
     *
     * @param int $id
     * @return bool
     */
    public function destroy($id)
    {
        $instance = $this->model->find($id);
        if ($instance) {
            $instance->delete();
            return true;
        }
        return false;
    }
}