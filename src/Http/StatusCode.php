<?php

namespace Hyrioo\LaravelHyperApi\Http;

class StatusCode {
    /**
     * Request OK
     */
    const OK = 200;
    /**
     * Resource created
     */
    const CREATED = 201;
    /**
     * Request accepted, but not yet completed processing
     */
    const ACCEPTED = 202;
    /**
     * Resource destroyed
     */
    const ACCEPTED_NO_CONTENT = 204;
    /**
     * Invalid request
     */
    const BAD_REQUEST = 400;
    /**
     * Requires login
     */
    const UNAUTHORIZED = 401;
    /**
     * Request is denied by policy
     */
    const FORBIDDEN = 403;
    /**
     * Given resource id is invalid
     */
    const NOT_FOUND = 404;
    /**
     * Given method is not allowed for this endpoint
     */
    const METHOD_NOT_ALLOWED = 405;
    /**
     * Validator deems input invalid
     */
    const UNPROCESSABLE_ENTITY = 422;
    /**
     * Error in code
     */
    const INTERNAL_SERVER_ERROR = 500;
    /**
     * Not implemented yet
     */
    const NOT_IMPLEMENTED = 501;
}
