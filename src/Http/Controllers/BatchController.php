<?php


namespace Hyrioo\LaravelHyperApi\Http\Controllers;


use GuzzleHttp\Client;
use function GuzzleHttp\Promise\settle;
use Hyrioo\LaravelHyperApi\Exceptions\ApiException;
use Hyrioo\LaravelHyperApi\Http\StatusCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class BatchController extends ApiController
{
    public function handleBatch(Request $request)
    {
        $body = json_decode($request->getContent(), true);
        $requests = $body['requests'];
        $defaultHeaders = isset($body['headers'])?$body['headers']:[];


        $client = new Client(['base_uri' => 'nginx/api/']);
        $promises = [];
        foreach ($requests as $request) {
            $promises[$request['id']] = $this->getPromise($client, $request, $defaultHeaders);
        }
        $results = settle($promises)->wait();

        $output = [];
        foreach ($requests as $request) {
            $output[$request['id']] = json_decode($results[$request['id']]['value']->getBody()->getContents());
            $output[$request['id']]->_status_code = $results[$request['id']]['value']->getStatusCode();
        }
        return Response::json(['responses' => $output]);
    }

    private function getPromise(Client $client, $request, $defaultHeaders)
    {
        $headers = array_merge($defaultHeaders, isset($request['headers'])?$request['headers']:[], ['X-BATCH-REQUEST' => true]);
        $url = ltrim($request['url'], '/');

        if($request['method'] === 'GET'){
            return $client->getAsync($url, [
                'verify' => false,
                'headers' => $headers
            ]);
        } else if($request['method'] === 'POST'){
            return $client->postAsync($url, [
                'verify' => false,
                'headers' => $headers,
                'json' => isset($request['body'])?$request['body']:null,
            ]);
        } else if($request['method'] === 'DELETE'){
            return $client->deleteAsync($url, [
                'verify' => false,
                'headers' => $headers,
                'json' => isset($request['body'])?$request['body']:null,
            ]);
        } else if($request['method'] === 'PATCH'){
            return $client->patchAsync($url, [
                'verify' => false,
                'headers' => $headers,
                'json' => isset($request['body'])?$request['body']:null,
            ]);
        } else if($request['method'] === 'PUT'){
            return $client->putAsync($url, [
                'verify' => false,
                'headers' => $headers,
                'json' => isset($request['body'])?$request['body']:null,
            ]);
        } else {
            throw new ApiException('Invalid request method', StatusCode::BAD_REQUEST, 'One or more of the batch requests contains an invalid request method');
        }
    }
}