<?php

namespace Hyrioo\LaravelHyperApi\Http\Controllers;

use function foo\func;
use Hyrioo\LaravelHyperApi\Exceptions\ApiException;
use Hyrioo\LaravelHyperApi\Http\StatusCode;
use Hyrioo\LaravelHyperApi\Models\ApiRequestSample;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\RelationNotFoundException;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\ValidationException;


class ApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	private static $reservedDataKeys = ['success', 'pagination'];
	private $paginate = false;
	private $pageSize = 20;
	private $pageOffset = 0;
	private $resultCount = 0;
	private $resultFilteredCount = 0;

	protected $request;
	private $isBatchRequest;

	private $startTime = null;
	private $sampleLogged = false;
	private $sampleData = [];

	/**
	 * ApiController constructor.
	 * @param Request|\Illuminate\Support\Facades\Request $request
	 */
	public function __construct(Request $request)
	{
		$this->request = Route::getCurrentRequest() ?: $request;

		$this->isBatchRequest = $this->request->header('X-BATCH-REQUEST') === true;

		$sample = config('api.sample-api-execution-time');
		if ($sample != false && random_int(1, $sample) === $sample) {
			$this->startTime = microtime(true);
		}
	}

	/**
	 * Process the query with expanding with relations, and filtering and ordering if the query should be paginated
	 * @param Builder $query
	 * @param array $options
	 * @return Collection|Model
	 */
	public function processQuery($query, $options)
	{
		$this->processOptions($query, $options);
		$query = $this->withExpandObjects($query);

		if ($this->paginate) {
			return $this->paginateQuery($query);
		} else {
			return $query->take(1)->get()->first();
		}
	}

	/**
	 * A shorthand for calling processQuery with paginate=true option
	 * @see processQuery
	 * @param $query
	 * @param array $options
	 * @return Collection|Model
	 */
	public function processQueryWithPagination($query, $options = [])
	{
		$opt = array_merge($options, [
			'paginate' => true,
		]);

		return $this->processQuery($query, $opt);
	}

	/**
	 * @param Builder $query
	 * @param $options
	 * @return array
	 */
	private function processOptions($query, $options)
	{
		$opt = [
			'paginate' => false,
			'pageSize' => $query->getModel()->getPerPage()
		];
		$opt = array_merge($opt, $options);

		$this->pageSize = $opt['pageSize'];
		$this->paginate = $opt['paginate'];

		return $opt;
	}


	//////////////////////
	// ERROR HANDLING

	public function callAction($method, $params)
	{
		try {
			return parent::callAction($method, $params);
		} catch (\Exception $exception) {
			$this->logExecutionTime();
			return $this->handleException($exception);
		}
	}

	/**
	 * Handles exceptions before it's eventually sent to Laravel's exception handler
	 * @param \Exception $exception
	 * @throws \Exception
	 */
	protected function handleException(\Exception $exception)
	{
		$exceptionType = get_class($exception);

		if($exceptionType == AuthenticationException::class)
			return ApiController::sendException(new ApiException('Login to API server failed.', StatusCode::UNAUTHORIZED, 'Be sure you have called the auth endpoint to require an access token'));

		if($exceptionType == RelationNotFoundException::class)
			return ApiController::sendException(new ApiException('Relation not found.', StatusCode::BAD_REQUEST, 'Check for typos in the expand parameter'));

		if($exceptionType == ValidationException::class)
			return app()->make('Illuminate\Contracts\Debug\ExceptionHandler')->render($this->request, $exception);

		/*if($exceptionType == NotFoundHttpException::class)
			ApiController::sendException($request, ['message' => 'Resource not found.', 'developer_hint' => 'Check for mistypes in the url.'], 404);*/
		/*if(get_class($exception) == NotPermittedException::class)
			ApiController::sendException($request, ['message' => 'Access not permitted.', 'details' => $exception->getMessage(), 'developer_hint' => 'Be sure the user has the right permissions.'], 403);
*/

		if(get_class($exception) == ApiException::class)
			return self::sendException(/** @var ApiException $exception */$exception);

		return ApiController::sendException(new ApiException('An internal server error has happened', StatusCode::INTERNAL_SERVER_ERROR, 'This error is most likely not caused by you. Please contact support to get additional details', ['exception_type' => $exceptionType, 'exception_message' => $exception->getMessage(), 'exception_stacktrace' => $exception->getTrace()]));
	}


	public static function handleExceptions(\Exception $exception)
	{
		if(get_class($exception) == ApiException::class) {
			/** @var ApiException $exception */
			return self::sendException($exception);
		} else {
			throw $exception;
		}
	}


	//////////////////////
	// SAMPLE

	/**
	 * Adds data to the execution sample record
	 * @param array $data
	 */
	public function setSampleData($data)
	{
		$this->sampleData = $data;
	}

	/**
	 * Calculated execution time, and add record to the database
	 */
	protected function logExecutionTime()
	{
		if ($this->startTime == null || $this->sampleLogged)
			return;

		ApiRequestSample::create(array_merge($this->sampleData, [
			'api_execution_time' => (microtime(true) - $this->startTime) * 1000,
            'total_execution_time' => (microtime(true) - LARAVEL_START) * 1000,
			'method' => $this->request->route()->action['controller'],
			'url' => urldecode($this->request->fullUrl()),
		]));

		$this->sampleLogged = true;
	}


	//////////////////////
	// RESPONSE

	/**
	 * Pack and send response with the correct data, statusCode and pagination
	 * @param array $data
	 * @param int $statusCode
	 * @return \Illuminate\Http\JsonResponse
	 */
	protected function sendSuccess($data = [], $statusCode = StatusCode::OK)
	{
		return $this->sendJsonResponse(true, $data, $statusCode);
	}

	/**
	 * Pack and send response with the correct data, statusCode and pagination
	 * @param array $data
	 * @param int $statusCode
	 * @return \Illuminate\Http\JsonResponse
	 */
	protected function sendFailed($data = [], $statusCode)
	{
		return $this->sendJsonResponse(false, $data, $statusCode);
	}

	/**
	 * Send json response append with pagination if success and paginate is true
	 * @param bool $success
	 * @param array $data
	 * @param int $statusCode
	 * @return \Illuminate\Http\JsonResponse
	 * @throws ApiException
	 */
	protected function sendJsonResponse($success, $data, $statusCode)
	{
		if (array_keys_exist($data, self::$reservedDataKeys)) {
			throw new ApiException('Data contains reserved keys', StatusCode::INTERNAL_SERVER_ERROR, 'An internal error has happened. Please contact support.');
		}

		if($success && $this->paginate){
			$data['pagination'] = [
				'page_size' => $this->pageSize,
				'page_offset' => $this->pageOffset,
				'results' => $this->resultCount,
				'results_filtered' => $this->resultFilteredCount,
				'pages' => ceil($this->resultFilteredCount / $this->pageSize),
				'prev_page' => $this->getPrevPageUrl(),
				'next_page' => $this->getNextPageUrl(),
				'last_page' => $this->getLastPageUrl(),
			];
		}
		$data['success'] = $success;

		$this->logExecutionTime();

		return response()->json($data, $statusCode);
	}

	/**
	 * Send an ApiException as json response
	 * @param ApiException $exception
	 */
	public static function sendException(ApiException $exception)
	{
		response()->json($exception->data, $exception->httpStatusCode)->send();
	}

	/**
	 * Send successful response if $obj is not null, otherwise send failed response.
	 * If $obj is null statusCode will be set to Not found.
	 * @param * $obj
	 * @param array $data
	 * @param int $statusCode
	 * @return \Illuminate\Http\JsonResponse
	 */
	protected function checkAndSend($obj, $data, $statusCode = StatusCode::OK)
	{
		if ($obj != null && $obj != false) {
			return $this->sendSuccess($data, $statusCode);
		} else {
			return $this->sendFailed([], StatusCode::NOT_FOUND);
		}
	}


	//////////////////////
	// DATA

	/**
	 * Sets value to null for data where value is empty
	 * @param $data
	 * @return mixed
	 */
	protected function nullEmptyData($data)
	{
		foreach ($data as $key => $value) {
			if ($value == "")
				$data[$key] = null;
		}
		return $data;
	}

	/**
	 * Filter items where value is empty
	 * @param $data
	 * @return array
	 */
	protected function filterEmptyData($data)
	{
		return array_filter($data, function ($value) {
			return trim($value) != "";
		});
	}

	/**
	 * Get a subset containing the provided keys with values from the input data if they are in the request.
	 * @param $keys
	 * @return array
	 */
	public function onlyIfExist($keys)
	{
		$keys = is_array($keys) ? $keys : func_get_args();

		$results = [];

		$input = $this->all();

		foreach ($keys as $key) {
			if ($this->exists($key)) {
				$value = data_get($input, $key);
				if (gettype($value) === 'string' && trim($value) === '')
					$value = null;
				Arr::set($results, $key, $value);
			}
		}

		return $results;
	}


	//////////////////////
	// PAGINATION

	/**
	 * Executes a query while handling pagination, filtering and ordering
	 * @param Builder $query
	 * @return mixed
	 */
	private function paginateQuery($query)
	{
		$this->parsePagination();
		$this->resultCount = $query->count();
		$filteredQuery = $this->filterQuery($query);
		$this->resultFilteredCount = $filteredQuery->count();
		$orderedQuery = $this->orderQuery($filteredQuery);

		return $orderedQuery->skip($this->pageSize * $this->pageOffset)->take($this->pageSize)->get();
	}

	/**
	 * Parses pagination parameters from the request
	 */
	private function parsePagination()
	{
		if ($this->request->has('page_size'))
			$this->pageSize = intval($this->request->get('page_size'));

		if ($this->request->has('page_offset'))
			$this->pageOffset = intval($this->request->get('page_offset'));
	}

	/**
	 * Gets the full url for the previous page
	 * @return null|string
	 */
	private function getPrevPageUrl()
	{
		if ($this->pageOffset == 0)
			return null;
		$url = $this->request->url();
		$param = $this->request->all();
		$param['page_size'] = $this->pageSize;
		$param['page_offset'] = $this->pageOffset - 1;
		$url .= '?' . urldecode(http_build_query($param));
		return $url;
	}

	/**
	 * Gets the full url for the next page
	 * @return null|string
	 */
	private function getNextPageUrl()
	{
		if ($this->pageOffset >= floor($this->resultFilteredCount / $this->pageSize))
			return null;
		$url = $this->request->url();
		$param = $this->request->all();
		$param['page_size'] = $this->pageSize;
		$param['page_offset'] = $this->pageOffset + 1;
		$url .= '?' . urldecode(http_build_query($param));
		return $url;
	}

	/**
	 * Gets the full url for the last page
	 * @return string
	 */
	private function getLastPageUrl()
	{
		$url = $this->request->url();
		$param = $this->request->all();
		$param['page_size'] = $this->pageSize;
		$param['page_offset'] = floor($this->resultFilteredCount / $this->pageSize);
		$url .= '?' . urldecode(http_build_query($param));
		return $url;
	}


	//////////////////////
	// EXPAND

	/**
	 * Expands objects corresponding to the request expand parameter with the load function
	 * @param array|object $objs
	 * @return mixed
	 */
	protected function loadExpandObjects($objs)
	{
		$expand = $this->request->get('expand');
		if (!$expand)
			return $objs;
		if (is_array($objs)) {
			foreach ($objs as $obj)
				$this->expandObject($obj, $expand);
		} else {
			$this->expandObject($objs, $expand);
		}
		return $objs;
	}

	/**
	 * Expand an object corresponding to the expand parameter using the load function
	 * @param $obj
	 * @param $expand
	 */
	private function expandObject($obj, $expand)
	{
		$expands = array_map('camel_case', explode(',', $expand));
		$obj->load($expands);
	}

	/**
	 * Expands query corresponding to the request expand parameter using the with function
	 * @param Builder $query
	 * @return mixed
	 * @throws ApiException
	 */
	private function withExpandObjects($query)
	{
		$expand = $this->request->get('expand');
		if (!$expand)
			return $query;
		$expands = array_map('camel_case', explode(',', $expand));

		$model = $query->getModel();
		if(!method_exists($model, 'validateExpanding')){
			throw new ApiException("This model isn't expandable", StatusCode::BAD_REQUEST, "The model '".$this->getModelClassName($model)."' can't be expanded");
		}
		$model->validateExpanding($expands);

		return $query->with($expands);
	}


	//////////////////////
	// FILTER

	/**
	 * Adds filter to query corresponding to the request filter parameter.
	 * Supports and, or operators and filtering on relations properties
	 *
	 * @param \Illuminate\Database\Query\Builder $query
	 * @return mixed
	 * @throws ApiException
	 */
	private function filterQuery($query)
	{
		$filterString = $this->request->get('filter');
		if(!$filterString)
			return $query;

		$filters = preg_split('/(\$or:|\$and:)/', $filterString, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

		$filterProperties = [];
		foreach ($filters as $str) {
			if (preg_match('/(\w+?)\$/', $str, $m))
				$filterProperties[] = $m[1];
		}

		$model = $query->getModel();
		if(!method_exists($model, 'validateFiltering')){
			throw new ApiException("This model isn't filterable", StatusCode::BAD_REQUEST, "The model '".$this->getModelClassName($model)."' can't be filtered");
		}
		$model->validateFiltering($filterProperties);

		//All filtering must happen inside it's own where group to prevent overriding system rules
		$query->where(function ($insideQuery) use ($filters, $query) {

			$binder = '$and:';

			for($i=0;$i<sizeof($filters);$i++)
			{
				$filter = $filters[$i];

				$nested = $this->checkNestedFilter($filters, $i, $binder, $insideQuery, $query);

				if($nested != false) {
					$i = $nested['i'];
					$binder = $nested['binder'];
					$insideQuery = $nested['inside_query'];
				}else {
					$return = $this->applyFilter($filter, $binder, $insideQuery, $query);
					$binder = $return['binder'];
					$insideQuery = $return['inside_query'];
				}
			}
		});

		return $query;
	}

	private function checkNestedFilter($filters, $i, $binder, $insideQuery, $query)
	{
		if($filters[$i][0] == '(')
		{
			$whereType = $this->getWhereBinder($binder);
			$insideQuery = $insideQuery->$whereType(function($protectedQuery) use($filters, &$i, $query){
				$binder = '$and:';

				$return = $this->applyFilter($filters[$i], $binder, $protectedQuery, $query);
				$binder = $return['binder'];
				$protectedQuery = $return['inside_query'];

				for($i=$i+1;$i<sizeof($filters);$i++)
				{
					$filter = $filters[$i];

					$nested = $this->checkNestedFilter($filters, $i, $binder, $protectedQuery, $query);

					if($nested != false) {
						$i = $nested['i'];
						$binder = $nested['binder'];
						$protectedQuery = $nested['inside_query'];
					}else {
						$return = $this->applyFilter($filter, $binder, $protectedQuery, $query);
						$binder = $return['binder'];
						$protectedQuery = $return['inside_query'];
					}

					if($i >= sizeof($filters) || $filters[$i][strlen($filters[$i])-1] == ')')
					{
						break;
					}
				}
			});

			return ['binder' => $binder, 'inside_query' => $insideQuery, 'i' => $i];
		}

		return false;
	}

	private function applyFilter($filter, $binder, $insideQuery, $query)
	{
		$filter = trim($filter, '()');
		if ($filter == '$or:' || $filter == '$and:') {
			$binder = $filter;
		} else {

			$matches = explode(',', preg_replace("/[\$:]/", ",", $filter));
			$column = $matches[0];
			$operator = $this->convertToOperator($matches[1]);
			$value = $matches[2];

			if (strpos($column, '.') != false) {
				$relation = camel_case(explode('.', $column)[0]);
				$result = $this->scopeModelJoin($insideQuery, $query, $relation);
				$insideQuery = $result['query'];
				$column = $result['table'] . '.' . explode('.', $column)[1];
			} else {
				$column = $insideQuery->getModel()->getTable() . '.' . $column;
			}

			if ($operator == 'is-null')
			{
				$insideQuery=$insideQuery->whereNull($column);
			}
			elseif ($operator == 'is-not-null')
			{
				$insideQuery=$insideQuery->whereNotNull($column);
			}
			else
			{
				if ($operator == 'like') {
					if (strpos($value, '*') === false)
						$value = '%' . $value . '%';
					else
						$value = str_replace('*', '%', $value);
				}
				$whereType=$this->getWhereBinder($binder);
				$insideQuery=$insideQuery->$whereType($column,$operator,$value);
			}
			$binder = '$and:';
		}

		return ['binder' => $binder, 'inside_query' => $insideQuery];
	}


	private function getWhereBinder($binder)
	{
		if ($binder == '$and:') {
			return 'where';
		} else if ($binder == '$or:') {
			return 'orWhere';
		} else {
			throw new ApiException('Unsupported binder', StatusCode::BAD_REQUEST, 'The filter contains an invalid binder', ['error' => "'$binder' is not a valid binder"]);
		}
	}


	/**
	 * Automatically joins the relation based on keys retrieved from the model
	 * @param $insideQuery
	 * @param Builder $query
	 * @param $relation_name
	 * @param string $operator
	 * @param string $type
	 * @param bool $where
	 * @return array
	 */
	private function scopeModelJoin($insideQuery, $query, $relation_name, $operator = '=', $type = 'left', $where = false)
	{
		$relation = $query->getModel()->$relation_name();
		$table = $relation->getRelated()->getTable();
		if(get_class($relation) == BelongsTo::class) {
			$one = $relation->getQualifiedForeignKey();
			$two = $relation->getQualifiedOwnerKeyName();
		}else {
			$one = $relation->getQualifiedParentKeyName();
			$two = $relation->getQualifiedForeignKeyName();
		}

		$joinKey = $table.'-'.$one.'-'.$operator.'-'.$two.'-'.$type.'-'.$where;
		if(!in_array($joinKey, $this->joins)){
			$query->addSelect($insideQuery->getModel()->getTable().".*");
			$class = get_class($relation);
			if($class == BelongsTo::class || $class == HasMany::class) {
				$this->groupBy = $relation->getQualifiedParentKeyName();
			}
			$query->join($table, $one, $operator, $two, $type, $where);

			$this->joins[] = $joinKey;
		}

		return ['query' => $insideQuery, 'table' => $table];

	}

	/**
	 * Converts filter url operators to sql operators
	 * @param $syntax
	 * @return string
	 * @throws ApiException
	 */
	private function convertToOperator($syntax)
	{
		if ($syntax == 'eq')
			return '=';
		if ($syntax == 'ne')
			return '!=';
		if ($syntax == 'gt')
			return '>';
		if ($syntax == 'gte')
			return '>=';
		if ($syntax == 'lt')
			return '<';
		if ($syntax == 'lte')
			return '<=';
		if ($syntax == 'like')
			return 'like';
		if($syntax == 'is-null' || $syntax == 'is-not-null')
			return $syntax;


		throw new ApiException('Invalid operator', StatusCode::BAD_REQUEST, 'The filter contains an invalid operator', ['error' => "'$syntax' is not a valid operator"]);
	}


	//////////////////////
	// ORDER

	/**
	 * Order the query according to the request order parameter
	 * @param Builder $query
	 * @return mixed
	 * @throws ApiException
	 */
	private function orderQuery($query)
	{
		$orderString = $this->request->get('order');
		if (!$orderString)
			return $query;
		$orders = explode(',', $orderString);

		$model = $query->getModel();
		if(!method_exists($model, 'validateOrdering')){
			throw new ApiException("This model isn't orderable", StatusCode::BAD_REQUEST, "The model '".$this->getModelClassName($model)."' can't be ordered");
		}
		$model->validateOrdering($orders);

		foreach ($orders as $order) {
			if ($order[0] == '-') {
				$direction = 'desc';
				$column = substr($order, 1);
			} else {
				$direction = 'asc';
				$column = $order;
			}
			$query = $query->orderBy($column, $direction);
		}
		return $query;
	}


	//////////////////////
	// HELPERS

	private function getModelClassName($model)
	{
		return substr(strrchr(get_class($model), '\\'), 1);
	}
}