<?php

Route::post('/batch', ['uses' => '\Hyrioo\LaravelHyperApi\Http\Controllers\BatchController@handleBatch', 'middleware' => 'api'])->name('api.batch');