<?php

function array_keys_exist(array $array, $keys, $all=false)
{
    $count = 0;
    foreach ($keys as $key) {
        if (array_key_exists($key, $array)) {
            $count++;
        }
    }

    return $all ? count($keys) === $count : $count > 0;
}