<?php

namespace Hyrioo\LaravelHyperApi\Traits;

use Hyrioo\LaravelHyperApi\Exceptions\ApiException;
use Hyrioo\LaravelHyperApi\Http\StatusCode;

/**
 * Trait Orderable
 * @property array $orderable The attributes that can be filtered using the API
 * @package Hyrioo\LaravelHyperApi\Traits
 */
trait Orderable
{

    public static function validateOrdering($orderable)
    {
        return self::validateOrderingInternal((new static), $orderable);
    }

    public static function validateOrderingInternal($model, $orders)
    {
        $class = get_class($model);
        $orderable = $class::$orderable;

        if(!property_exists($model, 'orderable') || $orderable === false){
            throw new ApiException('Unallowed order', StatusCode::BAD_REQUEST, 'This model doesn\'t allow ordering');
        }else if($orderable === true){
            return true;
        }

        foreach ($orders as $order)
        {
            $order = ltrim($order, '-');
            if(!in_array($order, self::$orderable))
            {
                throw new ApiException('Unallowed order', StatusCode::BAD_REQUEST, 'You are either ordering on a protected property or have a typo', [
                    'error' => "Ordering not allowed for '$order'",
                    'allowed_orders' => self::$orderable
                ]);
            }
        }
    }
}