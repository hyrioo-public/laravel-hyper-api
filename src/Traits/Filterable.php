<?php

namespace Hyrioo\LaravelHyperApi\Traits;

use Hyrioo\LaravelHyperApi\Exceptions\ApiException;
use Hyrioo\LaravelHyperApi\Http\StatusCode;
use Illuminate\Database\Eloquent\Relations\Relation;

/**
 * Trait Filterable
 * @property array $filterable The attributes that can be filtered using the API
 * @package Hyrioo\LaravelHyperApi\Traits
 */
trait Filterable
{
    public static function validateFiltering($filters)
    {
        return self::validateFilteringInternal((new static), $filters);
    }


    private static function validateFilteringInternal($model, $filters)
    {
        $class = get_class($model);
        $filterable = $class::$filterable;


        if(!property_exists($model, 'filterable') || $filterable === false){
            throw new ApiException('Unallowed filter', StatusCode::BAD_REQUEST, 'This model doesn\'t allow filtering');
        }else if($filterable === true){
            return true;
        }

        foreach ($filters as $filter)
        {
            if(strpos($filter, '.') === false)
            {
                if(!in_array($filter, $filterable))
                {
                    throw new ApiException('Unallowed filter', StatusCode::BAD_REQUEST, 'You are either filtering on a protected property or have a typo', [
                        'error' => "Filtering not allowed for '$filter'",
                        'allowed_filters' => $filterable
                    ]);
                }
            }else{
                $partOne = explode('.', $filter)[0];
                $partTwo = explode('.', $filter)[1];

                if(self::validateFilteringInternal($model, [$partOne]))
                {
                    $rel = $model->$partOne();
                    if ($rel instanceof Relation) {
                        $model = $rel->getRelated();
                    }
                    return self::validateFilteringInternal($model, [$partTwo]);
                }
            }
        }

        return true;
    }
}