<?php

namespace Hyrioo\LaravelHyperApi\Traits;

use Hyrioo\LaravelHyperApi\Exceptions\ApiException;
use Hyrioo\LaravelHyperApi\Http\StatusCode;
use Illuminate\Database\Eloquent\Relations\Relation;

/**
 * Trait Expandable
 * @property array $expandable The relationships that can be expanded using the API. If true everything will be allowed
 * @package Hyrioo\LaravelHyperApi\Traits
 */
trait Expandable
{
    public static function validateExpanding($expands)
    {
        return self::validateExpandingInternal((new static), $expands);
    }

    private static function validateExpandingInternal($model, $expands)
    {
        $class = get_class($model);
        $expandable = $class::$expandable;

        
        if(!property_exists($model, 'expandable') || $expandable === false){
            throw new ApiException('Unallowed expand', StatusCode::BAD_REQUEST, 'This model doesn\'t allow expanding');
        }else if($expandable === true){
            return true;
        }

        foreach ($expands as $expand)
        {
            if(strpos($expand, '.') === false)
            {
                if(!in_array($expand, $expandable))
                {
                    throw new ApiException('Unallowed expand', StatusCode::BAD_REQUEST, 'You are either expanding on a protected relationship or have a typo', [
                        'error' => "Expanding not allowed for '$expand'",
                        'allowed_expands' => $expandable
                    ]);
                }
            }else{
                $partOne = explode('.', $expand)[0];
                $partTwo = explode('.', $expand)[1];

                if(self::validateExpandingInternal($model, [$partOne]))
                {
                    $rel = $model->$partOne();
                    if ($rel instanceof Relation) {
                        $model = $rel->getRelated();
                    }
                    return self::validateExpandingInternal($model, [$partTwo]);
                }
            }
        }

        return true;
    }
}