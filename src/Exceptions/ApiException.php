<?php

namespace Hyrioo\LaravelHyperApi\Exceptions;


use Exception;

class ApiException extends Exception
{
    public $data;

    /**
     * @var int
     */
    public $httpStatusCode;

    /**
     * ApiException constructor.
     * @param string $message General message explaining the error
     * @param int $httpStatusCode
     * @param string $developerHint Hint to the developers on how to resolve the error
     * @param array $data
     */
    public function __construct($message, $httpStatusCode, $developerHint, $data = [])
    {
        parent::__construct('API Exception: '.$message, null, null);

        $this->data = array_merge([
            'message' => $message,
            'http_status_code' => $httpStatusCode,
            'developer_hint' => $developerHint,
        ], $data);
        $this->httpStatusCode = $httpStatusCode;
    }
}