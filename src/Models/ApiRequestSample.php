<?php

namespace Hyrioo\LaravelHyperApi\Models;

use Illuminate\Database\Eloquent\Model;

class ApiRequestSample extends Model
{

    protected $fillable = ['api_execution_time', 'total_execution_time', 'method', 'url'];

    public function setUpdatedAt($value){ ; }
}